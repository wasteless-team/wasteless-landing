import React, {useCallback, useRef, useState} from 'react';
import {Button, Checkbox, Input, Form as AntForm, notification} from 'antd';
import {string, func} from 'prop-types';
import SendInBlueIcon from './SendInBlueIcon';
import ReCAPTCHA from 'react-google-recaptcha';
import qs from 'qs';

const Form = ({sendButtonClassName, successfullyRegistered}) => {

    const [form] = AntForm.useForm();
    const {getFieldValue} = form;
    const [sendingRequest, setSendingRequest] = useState(false);
    const [agreement, setAgreement] = useState(false);
    const [completedReCAPTCHA, setCompletedReCAPTCHA] = useState(false);
    const reCAPTCHARef = useRef(null);
    const onFinish = useCallback(async () => {

        setSendingRequest(true);
        await fetch('https://44b15054.sibforms.com/serve/MUIEALoKwoXZmYKWR6bh8xslGC9J8DFcCWrdpfmYw5M3laWrvxqkwpYvbLN8BSgpgp22ki15chQHQf2jzXtHw_EiUyPGHzWRDadq07K2m0_2wYWMYv94LgrjGGsGaBr10BZLwcv-Hjf7xoU1QmZv9Ua5Zuwr32sHmSSweqlMbG8gk8cze1yxio0OE50jkTnQ7DqHLeE38KXgXPry', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: qs.stringify({
                EMAIL: getFieldValue('email'),
                OPT_IN: '1',
                email_address_check: "",
                locale: 'pl',
                html_type: "html",
            }),
            mode: "no-cors"
        })
            .then(
                r => {
                    notification.success({
                        message: 'Pomyślnie zapisano!',
                        description: 'Poinformujemy drogą mailową, gdy aplikacja będzie gotowa!'
                    });
                    successfullyRegistered();
                }
            )
            .catch(e => notification.error({
                message: 'Coś poszło nie tak!',
                description: 'Prosimy spróbować ponownie za jakiś czas!'
            }))
            .finally(() => setSendingRequest(false));

    }, []);

    const onFinishedCaptcha = useCallback((v) => {
        if (v !== null) setCompletedReCAPTCHA(true);
    }, []);

    return (
        <>
            <AntForm
                form={form}
                layout="inline"
                className='form'
                onFinish={onFinish}
            >
                <AntForm.Item
                    className='email-form'
                    name='email'
                    rules={[
                        {
                            required: true,
                            message: 'Proszę wprowadzić prawidłowy adres email',
                            pattern: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                            min: 3
                        }
                    ]}
                >
                    <Input placeholder="Twój adres email"/>
                </AntForm.Item>
                <AntForm.Item className='hide-mobile'>
                    <Button className={`${sendButtonClassName}`}
                            disabled={!agreement || !completedReCAPTCHA}
                            htmlType="submit"
                            loading={sendingRequest}
                            type='primary'
                    >
                        Chcę zaproszenie
                    </Button>
                </AntForm.Item>
                <AntForm.Item className='full-width-form checkbox-form'>
                    <Checkbox checked={agreement} onChange={(e) => setAgreement(e.target.checked)}>Zgadzam
                        się na otrzymywanie newslettera oraz z regulaminem usługi</Checkbox>
                    <ReCAPTCHA
                        ref={reCAPTCHARef}
                        sitekey='6LeqzOUUAAAAAGJhsFs-HkNLQHNFExJs8PfWP4w0'
                        onChange={onFinishedCaptcha}
                        hl='pl'
                    />
                </AntForm.Item>
                <AntForm.Item className=' sendinblue-form'>
                    <div className="declaration-block-icon">
                        <SendInBlueIcon/>
                    </div>
                    <p>
                        We use Sendinblue as our marketing platform. By Clicking below to submit this form,
                        you
                        acknowledge that the information you provided will be transferred to Sendinblue for
                        processing in accordance with their
                        <a target="_blank" className="clickable_link"
                           href="https://www.sendinblue.com/legal/termsofuse/">terms of use</a>
                    </p>
                </AntForm.Item>
                <AntForm.Item className='hide-desktop form-submit'>
                    <Button className={`${sendButtonClassName}`}
                            disabled={!agreement || !completedReCAPTCHA}
                            loading={sendingRequest}
                            type='primary'
                            block
                            htmlType="submit"
                    >
                        Chcę zaproszenie
                    </Button>
                </AntForm.Item>

            </AntForm>
        </>
    )
};

Form.propTypes = {
    sendButtonClassName: string,
    successfullyRegistered: func
};

Form.defaultProps = {
    sendButtonClassName: '',
    successfullyRegistered: () => {
    }
};

export default Form;