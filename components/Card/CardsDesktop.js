import React from 'react';
import {Col, Row} from 'antd';
import {Card} from "./index";

const CardsDesktop = () => {

    return (
        <Row className='cards-container cards-container-desktop hide-md' justify='space-between'>
            <Col span={24} lg={5} className='card-column'>
                <Card cardImage={<img src='/images/cards/receipt.svg' alt='paragon'/>} cardText='Skanujesz paragon'/>
            </Col>
            <Col span={24} lg={5} className='card-column'>
                <Card cardImage={<img src='/images/cards/phone.svg' alt='telefon'/>}
                      cardText='Zapisujemy Twoje zakupy oraz ich daty przydatności do spożycia'/>
            </Col>
            <Col span={24} lg={5} className='card-column'>
                <Card cardImage={<img src='/images/cards/checklist.svg' alt='lista kontrolna'/>}
                      cardText='Zaznaczasz co zjadłeś, a co się zmarnowało'/>
            </Col>
            <Col span={24} lg={5} className='card-column'>
                <Card cardImage={<img src='/images/cards/cart.svg' alt='cart'/>}
                      cardText='Tworzymy spersonalizowaną listę na następne zakupy'/>
            </Col>
            <div className='card-line'/>
        </Row>
    )
};


export default CardsDesktop;