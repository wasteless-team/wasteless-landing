import React from 'react';
import CardsMobile from './CardsMobile';
import CardsDesktop from "./CardsDesktop";

const Cards = () => {


    return (
        <>
            <div className='hide-mobile'>
                <CardsDesktop/>
            </div>
            <div className='hide-desktop'>
                <CardsMobile/>
            </div>
        </>
    )

};

export default Cards;