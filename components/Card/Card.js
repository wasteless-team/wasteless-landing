import React from 'react';
import {element, oneOfType, string, bool} from 'prop-types';
import {Col} from 'antd';

const Card = ({cardImage, cardText, isMobile}) => {

    let mobileCardProps = {};

    if (isMobile) {
        mobileCardProps = {
            imageWrapper: {
                span: 6
            },
            textWrapper: {
                span: 18
            }
        }
    }

    return (
        <div className='card-container'>
            <Col {...mobileCardProps.textWrapper} className='card-text-wrapper'>
                {cardText}
            </Col>
            <Col {...mobileCardProps.imageWrapper} className='card-image-wrapper'>
                {cardImage}
            </Col>
        </div>
    );
};

Card.propTypes = {
    cardImage: element.isRequired,
    cardText: oneOfType([
        element,
        string
    ]).isRequired,
    isMobile: bool
};

Card.defaultProps = {
    isMobile: false
};

export default Card;