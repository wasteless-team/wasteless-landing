import React from 'react';
import Link from 'next/link';

const Navbar = () => {

    return (
        <nav className='navbar'>
            <div className='container'>
                <div className='navbar-content-wrapper'>
                    <div className='navbar-branding'>
                        <Link href='/'><a>
                            <img style={{width: 40}} src='images/wasteless-logo.png' alt='wasteless logo'/>
                        </a>
                        </Link>
                    </div>
                </div>
            </div>
        </nav>
    )

};

export default Navbar;

