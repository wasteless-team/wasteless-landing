import React, {useEffect} from 'react';
import '../assets/thank-you-page.less';
import Link from "next/link";
import ReactPixel from 'react-facebook-pixel';

const ThankYouPage = () => {

    useEffect(() => {
        ReactPixel.init('220903875893777');
        ReactPixel.track('CompleteRegistration', {});
    }, []);

    return (
        <div className='thank-you-page-container'>
            <div className='container'>
                <h1>Dziękujemy za zapisanie</h1>
                <h3>
                    Zapisałeś/-aś się na naszą listę
                </h3>
                <Link href='/'>
                    <a>
                        <h5 className='team-container'>
                            <img src='/images/wasteless-logo.png'/>
                            zespół wasteless
                        </h5>
                    </a>
                </Link>
            </div>
        </div>
    )
};

export default ThankYouPage;