import '../global.less';
import React, {useEffect} from 'react';
import TagManager from 'react-gtm-module';
import ReactPixel from 'react-facebook-pixel';
import Head from 'next/head';


const tagManagerArgs = {
    gtmId: 'GTM-MD8TTVR',
};

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({Component, pageProps}) {

    useEffect(() => {
        TagManager.initialize(tagManagerArgs);
        ReactPixel.init('220903875893777');
        ReactPixel.pageView();
    }, []);

    return (
        <>
            <Head>
                <meta charSet="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
                <title>Wasteless – Share fridge, save food</title>

                <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png"/>
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
                <link rel="manifest" href="/site.webmanifest"/>
                <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5"/>
                <meta name="msapplication-TileColor" content="#da532c"/>
                <meta name="theme-color" content="#ffffff"/>

                <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700&display=swap"
                      rel="stylesheet"/>
            </Head>
            <Component {...pageProps} />
        </>
    );
}