import Head from 'next/head';
import React, {useCallback, useState} from 'react';
import {Navbar} from '../components/Navbar';
import {Col, Row} from 'antd';
import {Cards} from '../components/Card';
import {Form} from '../components/Form';

const Home = () => {

    const [hideForm, setHideForm] = useState(false);

    const successfullyRegistered = useCallback(() => {
        setHideForm(true);
    }, []);

    return (
        <div>
            <Navbar/>
            <main className='jumbotron'>
                <div className="container content-container">
                    <h1 className='main-title'>
                        Rocznie w Polsce marnujemy <br/> 9 031 050 000 kg żywności
                    </h1>
                    <h2 className='subtitle'>
                        tworzymy aplikację dla społeczności less waste mającą pomóc w zmniejszeniu tej liczby
                    </h2>
                    {!hideForm && <Row justify="center" className='row-form'>
                        <Col xs={{ span: 24 }} xl={{ span: 18 }}>
                            <Form sendButtonClassName='main-form' successfullyRegistered={successfullyRegistered}/>
                        </Col>
                    </Row>}
                </div>
            </main>
            <div className='grey-background'>
                <Row className='container content-container' align='middle'>
                    <Col xs={24} md={{ span: 11 }}>
                        <img src='/images/corn.png' alt='sałatka' id='salad'/>
                        <img src='/images/iphone.png' alt='obrazek telefonu Iphone' id='iphone'/>
                    </Col>
                    <Col xs={24} md={{ span: 11, offset: 2 }}>
                        <h1 className='main-title'>
                            Wasteless.io
                        </h1>
                        <h3 className='small-text'>
                            Aplikacja, która zapisuje informacje o Twoich zakupach, analizuje Twoje potrzeby, a
                            następnie
                            pomaga Ci ograniczyć marnowanie żywności.
                        </h3>
                    </Col>
                </Row>
            </div>
            <div className='container'>
                <div className='content-container'>
                    <h1 className='main-title'>Jak to działa?</h1>
                    <Cards/>
                </div>
            </div>
            <div className='container'>
                <div className='content-container'>
                    <h1 className='main-title'>
                        Podoba Ci się pomysł? Twoim znajomym może też się spodoba!
                    </h1>
                </div>
            </div>
            <div className='container'>
                <iframe className='form-container'
                        src="https://docs.google.com/forms/d/e/1FAIpQLSfSt9fmg7ITWBhJNnIdUCaaMoERqoe3WdnyXY3hEZa58en6Kg/viewform?embedded=true"
                        width="100%" frameBorder="0" marginHeight="0" marginWidth="0"
                >Loading…
                </iframe>
            </div>
            <div className='container'>
                <footer>
                    <a href='#' className='branding'> <img style={{ width: 80 }} src='images/wasteless-logo.png'
                                                           alt='wasteless logo'/>
                    </a>
                    <div className='footer-text'>
                        Masz pytania? Chętnie pogadamy na <a href='mailto:team@wasteless.io'>team@wasteless.io</a><br/>

                        <a href={'/polityka_prywatnosci.pdf'}>Polityka prywatności oraz cookies</a>

                    </div>
                </footer>
            </div>
        </div>
    );
};
export default Home;
